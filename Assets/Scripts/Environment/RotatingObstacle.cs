﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObstacle : MonoBehaviour {
    Quaternion rotation;
    Quaternion targetRotation;
    bool rotate = false;


    public int hitsToTurn = 5;
    public int hits = 0;
    public float degrees;

    [SerializeField]
    Rigidbody2D rb;

    float timer = 0;

	// Use this for initialization
	void Awake () {
        rb = GetComponent<Rigidbody2D>();
        //rotation = Quaternion.AngleAxis(degrees, Vector3.forward);
        //targetRotation = transform.rotation * rotation;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (rotate == true) {
            //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 6);

            //Using Rigidbody.MoveRotation to have less / no unintended collision behavior while rotating.
            //This function works in some weird way, seems it rotates double the given degree so for 90deg rotate, the value is 45.
            //It's also kinda inaccurate, got the desired rotations using Mathf.Round

            rb.MoveRotation(Mathf.Round(rb.rotation + degrees * Time.deltaTime));
            hits = 0;
            timer += Time.deltaTime;
        }

        if (timer >= 1.5f) {
            rotate = false;
           // targetRotation = transform.rotation * rotation;
            timer = 0;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ShieldProjectile")) {
            hits++;
            if (hits > hitsToTurn-1) {
                rotate = true;
            }
        }
    }
}
