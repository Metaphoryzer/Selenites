﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    private bool isRunning;
    private float currentTime;
    private float maxTime;

    public float CurrentTime
    {
        get { return currentTime; }
    }

    public bool IsRunning
    {
        get { return isRunning; }
    }

    public float MaxTime { get{ return maxTime; } }
	
	public void StartTimer (float value) {
        currentTime = 0;
        isRunning = true;
        maxTime = value;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (isRunning)
        {
            currentTime += Time.deltaTime;
        }
        
        if (currentTime >= maxTime)
        {
            StopTimer();
        }
	}

    public void StopTimer()
    {
        isRunning = false;
        
    }
}