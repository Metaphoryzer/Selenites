﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPauser : MonoBehaviour {


    AudioSource audioSource;
    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale == 0)
        {
            audioSource.Pause();
        }
        else
        {
            audioSource.UnPause();
        }
	}
}
