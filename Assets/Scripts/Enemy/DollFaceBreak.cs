﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollFaceBreak : MonoBehaviour {
    
    public float lifeTimer = 1;
    private AudioSource sound;
    private bool played;

    // Use this for initialization
    void Awake () {
        sound = GetComponent<AudioSource>();
        gameObject.SetActive(false);
        played = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!played)
        {
            sound.PlayOneShot(sound.clip);
            played = true;
        }

        lifeTimer -= Time.deltaTime;

        if (lifeTimer <= 0)
        {
            Destroy(gameObject);
        }
	}
}
