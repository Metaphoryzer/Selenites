﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrongDoll : MonoBehaviour {
	
	public Sprite[] dollSprites;
	private int health = 7;
	public GameObject eye;
	
	// How much score the player gets from killing this enemy
	private int _scoreOutput;
	public GameObject healthPickup;
	
	private AudioSource sound;
	public AudioClip crack;
	public AudioClip mama;
	[SerializeField]
	private int mamaSoundTime = 200000;
	
	private void Awake()
	{
		dollSprites = Resources.LoadAll<Sprite>("Mindy2");
		sound = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		int mamaGenerator = Random.Range(1, mamaSoundTime);
		if(mamaGenerator == 69 && Time.timeScale == 1)
		{
			sound.PlayOneShot(mama);
		}
	}
	
	// Detects collision with other 2D Colliders
    void OnCollisionEnter2D(Collision2D collision)
    {
        
        // When the enemy collides with the projectile, it destroys itself
        if (collision.gameObject.tag.Equals("ShieldProjectile") == true && Gamemanager.ShieldBounces < 1)
        {
            if (health > 1)
            {
                sound.PlayOneShot(crack);
                health--;
                GetComponent<SpriteRenderer>().sprite = dollSprites[health - 1];
                Instantiate(eye, transform.position, Quaternion.identity);
            }
            else
            {
                sound.PlayOneShot(crack);
                _scoreOutput = 10;
                Gamemanager.Score += _scoreOutput;
                if (GameObject.FindGameObjectsWithTag("HealthPickup").Length < 2)
                {
                    int i = Random.Range(1, 5);
                    Debug.Log(i);
                    if (i == 3)
                    {
                        Instantiate(healthPickup, transform.position, Quaternion.identity);
                    }
                }
         
                Destroy(gameObject);
            }
        }

        if (collision.gameObject.tag.Equals("ShieldProjectile") == true && Gamemanager.ShieldBounces > 0)
        {
            health -= Gamemanager.ShieldBounces;

            sound.PlayOneShot(crack);

            if (health > 0)
            {
                GetComponent<SpriteRenderer>().sprite = dollSprites[health - 1];
                Instantiate(eye, transform.position, Quaternion.identity);
            }
            else
            {
                _scoreOutput = 10 * Gamemanager.ShieldBounces;
                Gamemanager.Score += _scoreOutput;
                int i = Random.Range(1, 5);
                Debug.Log(i);
                if (i == 3)
                {
                    Instantiate(healthPickup, transform.position, Quaternion.identity);
                }

                Destroy(gameObject);
            }
        }
    }
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Explosion")
		{
			health -= 2;
			if (health > 0)
			{
				GetComponent<SpriteRenderer>().sprite = dollSprites[health - 1];
				Instantiate(eye, transform.position, Quaternion.identity);
			}
			else
			{
				int i = Random.Range(1, 5);
				Debug.Log(i);
				if (i == 3)
				{
					Instantiate(healthPickup, transform.position, Quaternion.identity);
				}

				Destroy(gameObject);
			}
		}  
	}
}

    