﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillCanvas : MonoBehaviour {

    public float destroyTime;
    
    void Start()
    {
        StartCoroutine(TimeToDie());

    }

    IEnumerator TimeToDie()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(destroyTime);

        Destroy(gameObject);
    }
}
