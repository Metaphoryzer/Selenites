﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawner : MonoBehaviour
{

	public GameObject enemy;	   // The gameobject to spawn
	private int enemiesLeft;
	
	public int minDistance;        // The minimum distance the player should be from the spawner
	private Transform target;      // Player position
	private Transform myTransform; // Spawner position
	
	[SerializeField, Tooltip("The time to wait when a wave ends before starting next wave")]
	private float waveWait;
	private float waveTimer;
	private int myWave;
	private bool newWave;

	private bool done = true;
	private GameObject g;
	

	// Use this for initialization
	void Start ()
	{
		g = GameObject.Find("SpawnerHandler");
		myTransform = transform;
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		target = player.transform;
		myWave = 0;
		newWave = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (myWave < Gamemanager.CurrentWave)
		{
			myWave++;
			newWave = true;
			waveTimer = 0;
		}

		waveTimer += Time.deltaTime;

		if (waveTimer >= waveWait)
		{

			if (Gamemanager.BombBool)
			{
				done = false;
			}

			if (!done && (Vector3.Distance(target.position, myTransform.position) > minDistance))
			{
				//Debug.Log("Spawning a bomb");
				Instantiate(enemy, transform.position, Quaternion.identity);
				g.GetComponent<WaveManager>().BombCount();
				done = true;
			}
		}
	}
}
