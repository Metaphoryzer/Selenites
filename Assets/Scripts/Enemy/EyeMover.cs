﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeMover : MonoBehaviour {

    private int _bounces;
    Rigidbody2D rb;
    public float timeToDie;        // Time in seconds until the eye dies
    private float timer;

    private void Awake()
    {
        //Physics2D.IgnoreLayerCollision(9, 9, true);
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        rb.AddForce((Gamemanager.PlayerTransform.position - transform.position).normalized * 3, ForceMode2D.Impulse);
        _bounces = 0;
    }

    void Update()
    {
        transform.eulerAngles = new Vector3(0, 0, GetAngle(rb.velocity));

        timer += Time.deltaTime;

        if(timer >= timeToDie)
        {
            Destroy(gameObject);
        }
    }

    float GetAngle(Vector2 movementVector)
    {
        float x = movementVector.x;
        float y = movementVector.y;
        return Mathf.Atan2(y, x) * Mathf.Rad2Deg;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _bounces++;

        if (collision.gameObject.tag.Equals("ShieldProjectile"))
        {
            Gamemanager.Score += 10;
            Destroy(gameObject);
        }
    }
}
