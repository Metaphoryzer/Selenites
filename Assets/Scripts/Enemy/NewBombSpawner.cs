﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBombSpawner : MonoBehaviour {

	public GameObject enemy;	   // The gameobject to spawn
	
	public int minDistance;        // The minimum distance the player should be from the spawner
	private Transform target;      // Player position
	private Transform myTransform; // Spawner position
	
	[SerializeField, Tooltip("The time to wait before spawning")]
	private float waitTime;
	private float timer;
	private bool newWave;
	
	private GameObject g;
	
	void Start ()
	{
		myTransform = transform;
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		target = player.transform;
		newWave = false;
		g = GameObject.Find("SpawnerHandler");
		
	}

	private void Update()
	{
		timer += Time.deltaTime;
		
		if (timer >= waitTime && Gamemanager.BombBool)
		{
			if ((Vector3.Distance(target.position, myTransform.position) > minDistance))
			{
				//Debug.Log("Spawning a bomb");
				Instantiate(enemy, transform.position, Quaternion.identity);
				timer = 0;
				g.GetComponent<BombManager>().BombCount();
			}
		}
	}
}
	
	
