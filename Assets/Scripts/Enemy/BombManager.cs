﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour {

    private int amountOfBombers; // The amount of bomb spawners in the scene
    private int bombsLeft;   // The amount of bomb enemies left
    private int bombCounter = 0; // A counter to keep track of how many bombs have finished spawning
    
    void Start ()
    {
        amountOfBombers = GameObject.FindGameObjectsWithTag("Spawner").Length;
    }

    private void Update()
    {
        bombsLeft = GameObject.FindGameObjectsWithTag("BombEnemy").Length;
        if (bombsLeft == 0)
        {
            Gamemanager.BombBool = true;
        }
    }
    
    public void BombCount()
    {
        bombCounter++;
        if (bombCounter == amountOfBombers)
        {
            Gamemanager.BombBool = false;
            bombCounter = 0;
        }
    }
}
