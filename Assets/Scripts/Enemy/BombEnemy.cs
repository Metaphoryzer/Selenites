﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombEnemy : MonoBehaviour {

    //public int maxX;
    //public int minX;
    private bool moveRight = true;
    public int speed = 3;
    public GameObject explosion;
	// Use this for initialization
	void Awake () {
        
    }
	
	// Update is called once per frame
	void Update () {

        if(moveRight)
        {
            transform.Translate(new Vector2(1,0) * speed * Time.deltaTime);
        }
        else if (!moveRight)
        {
            transform.Translate(new Vector2(-1,0) * speed * Time.deltaTime);
        }
        /*if(transform.localPosition.x >= maxX || transform.localPosition.x <= minX)
        {
            Flip();
        }*/
	}

   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "ShieldProjectile" || collision.gameObject.tag == "Player")
        {
            Gamemanager.Score += 10;
            
            Die();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Explosion")
        {
            Die();
        }
        if(other.gameObject.tag == "BombStopper")
        {
            Flip();
        }
    }
    private void Flip()
    {
        moveRight = !moveRight;
        Vector3 enemyScale = transform.localScale;
        enemyScale.x *= -1;
        transform.localScale = enemyScale;
    }

    private void Die()
    {
        explosion.SetActive(true);
        transform.DetachChildren();
        Destroy(transform.parent.gameObject);

    }
}
