﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreText : MonoBehaviour {

    public int _speed;

    private void Update()
    {
        float y = Time.deltaTime * _speed;
        transform.Translate(0, y, 0);
    }

    public void SetText(string score)
    {
        GetComponent<TextMeshProUGUI>().text = score;
    }

}
