﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour // This spawner spawns an X amount of enemies every time the player kills all the enemies
{
    private int dollsLeft;       // The amount of doll enemies left
    private int bombsLeft;       // The amount of bomb enemies left
    private int maxEnemies;         // The amount of enemies to spawn
    private int enemiesSpawned;    // The amount of enemies spawned so far
    public GameObject enemy;       // The enemy object that is spawned

    public int minDistance;        // The minimum distance the player should be from the spawner
    private Transform target;      // Player position
    private Transform myTransform; // Spawner position

    [SerializeField, Tooltip("The time to wait when a wave ends before starting next wave")]
    private float waveWait;
    private float waveTimer;
    private float timer;
    public float limit = 1f;       // The amount of time in seconds the spawner waits between each spawned enemy
    private bool startSpawn = false;

    private int myWave = 1;
    private bool spawn = false;
    private bool newWave;

    private GameObject g;

    // Set the spawner's position to the gameobject's current position
    void Awake()
    {
        myTransform = transform;

    }

    // Find the player object and find its position, set the number of enemies spawned to 0
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;

        g = GameObject.Find("SpawnerHandler");

        myWave = 0;
        newWave = false;
        enemiesSpawned = 0;
        maxEnemies = 2;
    }

    // With each frame the spawner checks if the scene is empty of both types of enemies
    // If the scene is empty, the spawner will begin spawning doll enemies until maxEnemies has been reached
    // The timer is used to space out the spawning, so the enemies don't all spawn at the same time
    void Update()
    {
        if(myWave < Gamemanager.CurrentWave)
        {
            Debug.Log("WAVE INCREASE");
            if (Gamemanager.DollsToSpawn > 0)
            {
                maxEnemies = Gamemanager.DollsToSpawn;
            }

            myWave++;
            newWave = true;
            waveTimer = 0;
        }

        waveTimer += Time.deltaTime;
        if (waveTimer >= waveWait)
        {

            dollsLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;
            bombsLeft = GameObject.FindGameObjectsWithTag("BombEnemy").Length;

            if (dollsLeft == 0 && bombsLeft == 0)
            {
                startSpawn = true;
            }

            if (startSpawn)
            {
                timer += Time.deltaTime;

                if ((Vector3.Distance(target.position, myTransform.position) > minDistance) && timer >= limit)
                {
                    Instantiate(enemy, transform.position, Quaternion.identity);
                    enemiesSpawned++;
                    timer = 0f;
                }
            }

            if (enemiesSpawned >= maxEnemies)
            {
                startSpawn = false;
                enemiesSpawned = 0;
                spawn = false;
                //newWave = false;
                g.GetComponent<WaveManager>().Count();
                Debug.Log("End spawning");
            }
        }
    }
}
