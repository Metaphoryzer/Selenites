﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

    [SerializeField, Tooltip("The amount of enemies to spawn during the first phase")]
    private int startingEnemies;
    private int enemiesToSpawn; // The total amount of enemies to spawn during this phase
    private int amountOfSpawners; // The amount of Doll Spawners in the scene
    private int amountOfBombers; // The amount of bomb spawners in the scene

    private int enemiesPerSpawner; // The amount of enemies ONE spawner spawns during this phase

    private int currentPhase;
    [Tooltip("How many phases (mini waves of enemies) within a wave")]
    [SerializeField]
    private int phasesPerWave;

    [Tooltip("How many more enemies to spawn per wave")]
    public int enemyMultiplier;

    private int enemiesLeft; // The amount of enemies left in the scene
    private int bombsLeft;   // The amount of bomb enemies left

    private bool dollDone = false;
    private bool _bombDone = false;
    private int counter = 0; // A counter to keep track of how many doll spawners have finished spawning
    private int bombCounter = 0; // A counter to keep track of how many bombs have finished spawning


	void Start ()
	{
	    amountOfSpawners = GameObject.FindGameObjectsWithTag("DollSpawner").Length;
	    amountOfBombers = GameObject.FindGameObjectsWithTag("Spawner").Length;
        enemiesToSpawn = startingEnemies;
        currentPhase = 0;
	    Gamemanager.CurrentWave = 1;
	}
	
    // If there are no enemies left, InitiateSpawn ONCE
    // The bools dollDone and _bombDone are used to prevent the function from being triggered multiple times
	void Update () {

        enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;
	    bombsLeft = GameObject.FindGameObjectsWithTag("BombEnemy").Length;
        if (enemiesLeft == 0 && bombsLeft == 0 && !dollDone && !_bombDone)
        {
            Gamemanager.BombBool = true;
            InitiateSpawn();
            dollDone = true;
            _bombDone = true;
        }

    }

    // Keeps track of the current phase and wave
    // If currentPhase = phasesPerWave, then the current wave is increased
    // If the wave is increased, the amount of enemies to spawn is updated
    public void InitiateSpawn()
    {
        if (currentPhase == phasesPerWave)
        {
            Gamemanager.CurrentWave++;
            currentPhase = 0;

            enemiesToSpawn = enemiesToSpawn + enemyMultiplier * Gamemanager.CurrentWave;
            enemiesPerSpawner = enemiesToSpawn / amountOfSpawners;
        }

        else { currentPhase++; Debug.Log("Phase: " + currentPhase); }

        Gamemanager.DollsToSpawn = enemiesPerSpawner;
        Debug.Log("Dolls to spawn: " + Gamemanager.DollsToSpawn);

        Debug.Log("Current wave: " + Gamemanager.CurrentWave);
    }

    // A function that is called from the Spawner script when a spawner has finished spawning dolls
    public void Count()
    {
        counter++;
        if (counter == amountOfSpawners)
        {
            counter = 0;
            dollDone = false;
        }
    }

    // A function that is called from the Bombspawner script when a bombspawner has finished spawning
    public void BombCount()
    {
        bombCounter++;
        if (bombCounter == amountOfBombers)
        {
            Gamemanager.BombBool = false;
            bombCounter = 0;
            _bombDone = false;
        }
    }
}
