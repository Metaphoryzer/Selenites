﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public class Spawner : MonoBehaviour // This spawner spawns an X amount of enemies every time the player kills all the enemies
{
    private int enemiesLeft;       // The amount of enemies left
    private int maxEnemies;         // The amount of enemies to spawn
    public GameObject enemy;       // The enemy object that is spawned

    public int minDistance;        // The minimum distance the player should be from the spawner
    private Transform target;      // Player position
    private Transform myTransform; // Spawner position
    private bool maxSpawn;         // A boolean for whether or not the spawner has spawned the max amount of enemies

    private int enemiesSpawned;



    void Awake()
    {
        myTransform = transform;

    }

    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        enemiesSpawned = 0;

        maxSpawn = false;
    }

    void Update()
    {
        if (Gamemanager.SpawnBool)
        {
            Spawn(Gamemanager.DollsToSpawn);
        }
        enemiesLeft = GameObject.FindGameObjectsWithTag("Enemy").Length;

        if (enemiesLeft == 0)
        {
            Spawn(Gamemanager.DollsToSpawn);
        }
    }

    public void Spawn(int amount)
    {
        //(Vector3.Distance(target.position, myTransform.position) > minDistance)

        maxEnemies = amount;
        Debug.Log("Enemies to spawn: " + amount);
        while (!maxSpawn)
        {
            StartCoroutine(SpawnTimeDelay());
        }
    }


    IEnumerator SpawnTimeDelay()
    {
        //yield return new WaitForEndOfFrame();
        for (int i = 0; i < maxEnemies; i++)
        {
            Instantiate(enemy, transform.position, Quaternion.identity);
            enemiesSpawned++;
            if (enemiesSpawned == maxEnemies)
            {
                maxSpawn = true;
            }
            Debug.Log("Enemies spawned: " + enemiesSpawned);
            yield return new WaitForSecondsRealtime(1);
        }
    }
}*/