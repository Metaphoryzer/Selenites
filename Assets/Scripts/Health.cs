﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {


    public float startingHealth = 100;
    public float currentHealth;
    public Slider healthSlider;
    public float maxHealth = 100;
    public float minHealth = 0;
    private bool isImmortal;

    // Use this for initialization
    void Start() {
        currentHealth = startingHealth;
        healthSlider.value = currentHealth;
    }

    public bool IsImmortal
    {
        get {return isImmortal;}
    }

    public void TakeDamage(float amount)
    {
        if (!isImmortal)
        {
            currentHealth -= amount;
            healthSlider.value = currentHealth;
        }
        if(currentHealth < minHealth)
        {
            currentHealth = minHealth;
        }

        IsDead();

    }

    public void Heal(float amount)
    {
        currentHealth += amount;
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        healthSlider.value = currentHealth;
    }

    public bool setImmortal(bool value)
    {
        isImmortal = value;
        return isImmortal;
    }

    public bool IsDead() {
        if (currentHealth == minHealth)
        {
            //Debug.Log("Dead");
            return true;
        }
        else
        {
            return false;
        }
    }


}
