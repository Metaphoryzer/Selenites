﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Gamemanager {

    private static int shieldBounces = 0;
    private static int score = 0;
    private static bool shieldVisible = false, playerVisible = true;
    private static int currentWave = 1;
    private static bool _bombBool = false;
    private static int dollsToSpawn = 0;

    public static int ShieldBounces {
        get { return shieldBounces; }
        set {
            shieldBounces = value;
            //Debug.Log("Bounces: " + shieldBounces);
        }
    }

    public static int Score
    {
        get { return score; }
        set {
            score = value;
            //Debug.Log("Score: " + score);
        }
    }
	
    public static bool ShieldVisible {
        get { return shieldVisible; }
        set { shieldVisible = value; }
    }

    public static bool PlayerVisible {
        get { return playerVisible; }
        set { playerVisible = value; }
    }

    public static Transform PlayerTransform { get; set; }
    public static Health PlayerHealth { get; set; }
    public static Camera Camera { get; set; }
    public static Vector2 CamBoundsX { get; set; }
    public static Vector2 CamBoundsY { get; set; }
    public static bool ShieldActive { get; set; }
    public static bool PlayerDead { get; set; }
    public static bool ShowUnlockBanner { get; set; }

    public static int CurrentWave
    {
        get { return currentWave; }
        set { currentWave = value; }
    }

    public static bool BombBool
    {
        get { return _bombBool; }
        set { _bombBool = value; }
    }

    public static int DollsToSpawn
    {
        get { return dollsToSpawn; }
        set { dollsToSpawn = value; }
    }
    

}
