﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactBlast : MonoBehaviour {

    ParticleSystem particle;
    float timer, maxS;
    Vector3 minSize;
    Vector3 maxSize;

    // Use this for initialization
    private void Awake()
    {
        particle = GetComponent<ParticleSystem>();
        Physics2D.IgnoreLayerCollision(10, 11, true);
        Physics2D.IgnoreLayerCollision(8, 11, true);
        
    }
    void OnEnable () {

        timer = 0;

        maxS = Gamemanager.ShieldBounces > 0 ? .5f + Gamemanager.ShieldBounces / 15 : .5f;
        if (maxS > 4) maxS = 4;
        maxSize = new Vector3(maxS, maxS, maxS);
        transform.localScale = maxSize;
        particle.time = 0;
        particle.Play();
    }
	
	// Update is called once per frame
	void Update () {
       
        timer += Time.deltaTime;

       
       
        if (timer >= .7f) {
            gameObject.SetActive(false);
        }
        
	}
    private void OnDisable()
    {
        particle.Stop();
        Gamemanager.ShieldBounces = 0;

    }

}
