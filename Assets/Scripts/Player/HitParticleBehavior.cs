﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitParticleBehavior : MonoBehaviour {

    float timer = 0;
    ParticleSystem [] particle;
    private void Awake()
    {
        particle = GetComponentsInChildren<ParticleSystem>();
    }



    private void OnDisable()
    {
        foreach(ParticleSystem part in particle) part.time = 0;
        timer = 0;
    }
    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= .25f) gameObject.SetActive(false);
        
           
    }
}
