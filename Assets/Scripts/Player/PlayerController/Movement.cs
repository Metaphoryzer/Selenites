﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour {
    CircleCollider2D collider;
    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer spriteRend;
    public Slider dashSlider;

    float colliderRadius;
    [SerializeField]
    float maxSpeedX, speedX, speedY, gravityMultiplier;
    float horizontalMove;
    float xInput;
    [SerializeField]
    private float stepSoundTime = 0.3f;
    private float stepTimer;
    [SerializeField]
    float jumpPower, dashSpeed, dashCooldownTime = 2;
    
    public bool Grounded, Falling;
    bool Running, dashCooldown = false, dashing = false;
    public LayerMask ground;
    bool wallRight, wallLeft;

    int maxJumps = 2;
    public int jumpsLeft;
    private bool landed;

    [SerializeField]
    private float floorCheckRadius, ceilingCheckRadius, circleCastOffset, boxSizeX, boxSizeY, boxCastOffsetY;
    Vector2 currentVelocity, lastFrameVelocity;

    private AudioSource sound;
    public AudioClip stepSound;
    public AudioClip jumpSound;
    public AudioClip landSound;


    Vector2 velocity, dashDir;
    [SerializeField, Tooltip("Smoothing values for movement on ground and in air")]
    float groundSmooth = 4, airSmooth = 10;
    float lastFrameMove, timer = 0;

    private void Awake()
    {
        sound = GetComponent<AudioSource>();
        collider = GetComponent<CircleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRend = GetComponent<SpriteRenderer>();
        colliderRadius = collider.radius;
        dashSlider.value = 100;
        stepTimer = stepSoundTime;
    }
    // Use this for initialization
    void Start () {
        
        jumpsLeft = maxJumps;
        
    }

    public float getInputX() {
        return xInput;
    }
    public float getHorizontalMove() {
        return horizontalMove;
    }

    public float getVerticalMove() {
        return speedY * Time.deltaTime;
    }
    public Vector2 getCurrentVelocity() {
        return currentVelocity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //ground check circle
        Gizmos.DrawWireSphere(transform.position + Vector3.up * circleCastOffset * transform.localScale.y, floorCheckRadius * transform.localScale.x);

        //ceiling check circle
        Gizmos.DrawWireSphere(transform.position + Vector3.up * circleCastOffset * transform.localScale.y * 3, ceilingCheckRadius * transform.localScale.x);

        //Jump buffer distance
        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * transform.localScale.y * .45f);

        //wall check boxcast representation
        Gizmos.DrawWireCube(transform.position + new Vector3(xInput * colliderRadius * transform.localScale.x, boxCastOffsetY * transform.localScale.y), new Vector3(boxSizeX * transform.localScale.x, boxSizeY * transform.localScale.y));
    }

    private void LateUpdate()
    {
        anim.SetBool("OnGround", Grounded);
        anim.SetBool("Falling", Falling);
        anim.SetBool("Running", Running);
        anim.SetBool("Dash", dashing);

    }

    public void Move()
    {


        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, transform.localScale.y * .45f, ground);
        if (hit.collider != null && Falling) {
            jumpsLeft = maxJumps;
        }
        
        /*
        anim.SetBool("OnGround", Grounded);
        anim.SetBool("Falling", Falling);
        anim.SetBool("Running", Running);
        */
        if (Grounded)
        {
            if(landed == false)
            {
                Land();
            }
            anim.SetBool("Jump", false);
            speedY = 0;
            jumpsLeft = maxJumps;
            //Debug.Log(hitCollider.name);
            if (speedX < maxSpeedX)
            {
                speedX = maxSpeedX;
            }
            Running = (xInput != 0 ? true : false);
            if (Running)
            {
                
                stepTimer-= Time.deltaTime;
                if (stepTimer <= 0)
                {
                    
                    sound.PlayOneShot(stepSound);
                    stepTimer = stepSoundTime;
                }
            }
            
        }
        else
        {
            speedY -= 9.81f * gravityMultiplier * Time.deltaTime;
            Running = false;
        }

        /*
        if (horizontalMove < 0 && !dashing) spriteRend.flipX = true;
        else if (horizontalMove > 0 && !dashing) spriteRend.flipX = false;
        */

        if (!Grounded && speedY < 0)
        {
            Falling = true;
            landed = false;
        }
        else Falling = false;

        xInput = Input.GetAxis("Horizontal");

        if (xInput != 0)
        {
            RaycastHit2D boxcastHit = Physics2D.BoxCast(transform.position + new Vector3(0,boxCastOffsetY * transform.localScale.y), new Vector2(boxSizeX * transform.localScale.x, boxSizeY * transform.localScale.y), 0f, new Vector2(xInput, + boxCastOffsetY * transform.localScale.y), colliderRadius * transform.localScale.x, ground);
            if (boxcastHit.collider != null)
            {

                wallRight = (xInput > 0 ? true : false);
                wallLeft = (xInput < 0 ? true : false);
                
                //Debug.Log("Osuu seinään tms");
            }
            else
            {
                wallRight = false;
                wallLeft = false;
            }
        }

        



        if ((wallRight && xInput > 0) || (wallLeft && xInput < 0))
        {
            horizontalMove = 0;
        }
        else
        {
            horizontalMove = xInput * speedX;
        }

        float smooth = (Grounded ? groundSmooth : airSmooth);

        speedY = Mathf.Clamp(speedY, -20, 20);

       
        currentVelocity = new Vector2(horizontalMove * transform.localScale.x, speedY);

        if (dashing)
        {
            currentVelocity = dashDir;
            if (Falling) speedY = 0;
            currentVelocity = currentVelocity * dashSpeed;
            smooth = 0;
            Physics2D.IgnoreLayerCollision(10, 9, true);

        }
        else {
            Physics2D.IgnoreLayerCollision(10, 9, false);
        }
        
        currentVelocity = Vector2.SmoothDamp(lastFrameVelocity, currentVelocity, ref velocity, smooth * transform.localScale.x * Time.deltaTime);
        lastFrameVelocity = currentVelocity;

        if (currentVelocity.x < 0) spriteRend.flipX = true;
        else if (currentVelocity.x > 0) spriteRend.flipX = false;
            
        
        if (dashing)
        {
            rb.MovePosition(rb.position + currentVelocity * Time.deltaTime);
        }
        else {
            rb.MovePosition(rb.position + new Vector2(currentVelocity.x * Time.deltaTime, speedY * Time.deltaTime));
        }

        if (wallRight && dashing || wallLeft && dashing) {
            dashing = false;
        }
        /*
        currentVelocity = Vector2.SmoothDamp(lastFrameVelocity, currentVelocity, ref velocity, smooth * Time.deltaTime);
        lastFrameVelocity = currentVelocity;

        
        rb.MovePosition(rb.position + new Vector2(currentVelocity.x * Time.deltaTime, speedY * Time.deltaTime));
        //rb.MovePosition(rb.position + new Vector2(currentVelocity.x * Time.deltaTime, currentVelocity.y * Time.deltaTime));
        */

        if (Input.GetButtonDown("Jump") && jumpsLeft > 0) Jump();
        if (!Grounded && speedY > 0 && Input.GetButtonUp("Jump") && jumpsLeft < maxJumps && jumpsLeft > 0) {
            speedY = speedY / 2;
        }


        if (Input.GetButtonDown("Fire1") && !dashCooldown && Mathf.Abs(xInput) >= .75f)
        {
            Dash();
        }

        if (dashCooldown) {
            timer += Time.deltaTime;
            dashSlider.value += dashSlider.maxValue / dashCooldownTime * Time.deltaTime;
            if (timer >= dashCooldownTime) {
                dashCooldown = !dashCooldown;
                timer = 0;
            }
        }

        Collider2D hitCollider = Physics2D.OverlapCircle(transform.position + Vector3.up * circleCastOffset * transform.localScale.y, floorCheckRadius * transform.localScale.x, ground);
        Grounded = (hitCollider == null ? false : true);
        

        if (speedY > 0)
        {
            hitCollider = Physics2D.OverlapCircle(transform.position + Vector3.up * circleCastOffset * transform.localScale.y * 3, ceilingCheckRadius * transform.localScale.x, ground);
            if (hitCollider != null) {
                speedY = (hitCollider.gameObject.layer != 8 && hitCollider.gameObject.layer != 9 ? speedY : 0);
            }
            
        }

        if (rb.velocity != Vector2.zero) rb.velocity = Vector2.zero;

        

    }


    public void Jump() {
        anim.SetBool("Jump", true);
        sound.PlayOneShot(jumpSound);
        transform.position = new Vector3(transform.position.x, transform.position.y + .2f);
        speedY = jumpPower;
        jumpsLeft--;
    }

    void Dash() {
        dashDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
        StartCoroutine(DashUtil());
        dashSlider.value = 0;
       
    }

    IEnumerator DashUtil() {
        
        dashing = true;
        dashCooldown = true;

        yield return new WaitForSeconds(.25f);

        
        dashing = false;
    }
    public bool Dashing { get { return dashing; } }

    void Land()
    {
        sound.PlayOneShot(landSound);
        landed = true;
    }

}
