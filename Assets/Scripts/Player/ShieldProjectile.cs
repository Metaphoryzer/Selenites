﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldProjectile : MonoBehaviour
{

    [SerializeField]
    private GameObject[] ImpactBlast;
    private GameObject[] effects;

    int index = 0;

    Rigidbody2D rb;
    Collider2D coll;
    float timer = 0;

    [SerializeField]
    private int bounces;

    [SerializeField]
    float minSpeed = 15, maxSpeed = 30;

    private AudioSource sound;
    public AudioClip bounceOne;
    public AudioClip bounceTwo;
    public AudioClip bounceThree;

    private int soundRandomizer;


    public int Bounces {
        get {
            return bounces;
        }
        set {
            bounces = value;
        }

    }


    void Awake()
    {
        Bounces = 0;
        coll = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
        effects = new GameObject[ImpactBlast.Length];
        sound = GetComponent<AudioSource>();


        int i = 0;
        foreach (GameObject go in ImpactBlast) {
            effects[i] = Instantiate(go, transform.position, transform.rotation);
            effects[i].SetActive(false);
            effects[i].transform.parent = transform.root;

            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
        else if (rb.velocity.magnitude < minSpeed) {
            rb.velocity = rb.velocity.normalized * minSpeed;
        }

        if (Gamemanager.ShieldActive != gameObject.activeSelf) Gamemanager.ShieldActive = gameObject.activeSelf;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*
        if (!collision.gameObject.CompareTag("ImpactBlast") && !collision.gameObject.CompareTag("Player")) {

            timer += Time.deltaTime;
            ImpactBlast[index].transform.position = transform.position;
            Physics2D.IgnoreCollision(coll, ImpactBlast[index].GetComponent<Collider2D>());
            ImpactBlast[index].SetActive(true);

            if (timer >= .6f)
            {
                Physics2D.IgnoreCollision(coll, ImpactBlast[index].GetComponent<Collider2D>(), false);
                timer = 0;
            }
            

            if (index < ImpactBlast.Length - 1 && index >= 0) index++;
            else index = 0;    
        }
        */

        if (!collision.gameObject.CompareTag("Player"))
        {


            effects[index].transform.position = transform.position;
            effects[index].SetActive(true);
           
            float angle = Mathf.Atan2(rb.velocity.x, rb.velocity.y) * Mathf.Rad2Deg;
            
            effects[index].transform.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);




            if (index < effects.Length - 1 && index >= 0) index++;
            else index = 0;


            if (!collision.gameObject.CompareTag("Player"))
            {
                Gamemanager.ShieldBounces++;
                timer += Time.deltaTime;

                Vector3 projectileDir = rb.velocity;
                projectileDir.Normalize();

                //ImpactBlast[index].transform.position = transform.position - (projectileDir * .2f);
                //Physics2D.IgnoreCollision(coll, ImpactBlast[index].GetComponent<Collider2D>());
                //ImpactBlast[index].SetActive(true);

                if (timer >= .2f)
                {
                    // Physics2D.IgnoreCollision(coll, ImpactBlast[index].GetComponent<Collider2D>(), false);
                    timer = 0;
                }


                if (index < effects.Length - 1 && index >= 0) index++;
                else index = 0;
            }
            if (!collision.gameObject.CompareTag("Player") && !collision.gameObject.CompareTag("Enemy"))
            {
                soundRandomizer = Random.Range(1, 4);
                if (soundRandomizer == 1)
                {

                    sound.PlayOneShot(bounceOne);
                }
                else if (soundRandomizer == 2)
                {
                    sound.PlayOneShot(bounceTwo);

                }
                else
                {
                    sound.PlayOneShot(bounceThree);

                }
            }


        }
    }
        private void OnCollisionExit2D(Collision2D collision)
        {

        }

        //For camera zoom
        private void OnBecameVisible()
        {
            Gamemanager.ShieldVisible = true;
        }

        private void OnBecameInvisible()
        {
            Gamemanager.ShieldVisible = false;
        }
    } 
