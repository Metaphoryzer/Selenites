﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour {

	public void Fade()
    {
        StartCoroutine(StartFade());
    }

    IEnumerator StartFade()
    {
        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.interactable = false;
            canvasGroup.alpha -= Time.deltaTime / 3;
            yield return null;
        }
        
        yield return null;
    }
}
