﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class MainMenu : MonoBehaviour {
    public EventSystem es;
    [SerializeField]
    GameObject playbutton, slider, options, mainmenu, levelSelect, score1, score2, score3, level1, level2;

    [SerializeField]
    ScoreTracker scoretracker;

    bool optionsVisible;
    bool levelSelectVisible;
    private AudioSource sound;
    public AudioClip startSound;

    private void Awake()
    {

        sound = GetComponent<AudioSource>();
        //es.firstSelectedGameObject = playbutton;
        optionsVisible = false;
        levelSelectVisible = false;
        Gamemanager.PlayerDead = false;

    }

    private void Start()
    {
        score1.GetComponent<TextMeshProUGUI>().text = "1: " + PlayerPrefs.GetInt("score1").ToString();
        score2.GetComponent<TextMeshProUGUI>().text = "2: " + PlayerPrefs.GetInt("score2").ToString();
        score3.GetComponent<TextMeshProUGUI>().text = "3: " + PlayerPrefs.GetInt("score3").ToString();
    }

    public void Options() {
        if (!optionsVisible)
        {
            mainmenu.SetActive(false);
            options.SetActive(true);
            es.SetSelectedGameObject(slider);
            optionsVisible = true;
        }
        else {
            mainmenu.SetActive(true);
            options.SetActive(false);
            es.SetSelectedGameObject(playbutton);
            optionsVisible = false;
        }

   
    }

    public void LevelSelect()
    {
        if (!levelSelectVisible)
        {
            mainmenu.SetActive(false);
            levelSelect.SetActive(true);
            es.SetSelectedGameObject(level1);
            levelSelectVisible = true;
        }
        else
        {
            mainmenu.SetActive(true);
            levelSelect.SetActive(false);
            es.SetSelectedGameObject(playbutton);
            levelSelectVisible = false;
        }

    }


    private void Update()
    {

        if (Time.timeScale != 1) Time.timeScale = 1;
        if (optionsVisible && Input.GetButtonDown("Cancel")) {
            Options();
        }
        if (levelSelectVisible && Input.GetButtonDown("Cancel"))
        {
            LevelSelect();
        }


        if (es.currentSelectedGameObject == null && Input.GetAxis("Vertical") != 0) {
            es.SetSelectedGameObject(optionsVisible ? slider : playbutton);
        }

        
    }

    public void Level1()
    {
        sound.PlayOneShot(startSound);
        if (Gamemanager.Score != 0) Gamemanager.Score = 0;
        if (Gamemanager.ShowUnlockBanner) Gamemanager.ShowUnlockBanner = false;
        Invoke("LoadLevel1", 4f);
    }

    public void Level2()
    {
        sound.PlayOneShot(startSound);
        if (Gamemanager.Score != 0) Gamemanager.Score = 0;
        if (Gamemanager.ShowUnlockBanner) Gamemanager.ShowUnlockBanner = false;
        Invoke("LoadLevel2", 4f);

    }

    void LoadLevel1()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
    }

    void LoadLevel2()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT GAME");
        Application.Quit();
    }
}
