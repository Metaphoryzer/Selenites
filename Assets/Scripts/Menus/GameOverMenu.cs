﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameOverMenu : MonoBehaviour {
    [SerializeField]
    EventSystem es;
    [SerializeField]
    GameObject restart, banner, mainmenu;

    private void Awake()
    {
        //es.firstSelectedGameObject = restart;
        if (Gamemanager.ShowUnlockBanner && !banner.activeSelf) banner.SetActive(true);
    }

    private void Update()
    {
        if ((es.currentSelectedGameObject != restart && es.currentSelectedGameObject != mainmenu) && Input.GetAxis("Vertical") != 0) {
            es.SetSelectedGameObject(restart);
        }
        
    }

    public void RestartLevel()
    {
        banner.SetActive(false);
        Gamemanager.Score = 0;
        if (Gamemanager.ShowUnlockBanner) Gamemanager.ShowUnlockBanner = false;
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    public void ToMainMenu()
    {
        banner.SetActive(false);
        SceneManager.LoadScene("Main_Menu");
    }
}
