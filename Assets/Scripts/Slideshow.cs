﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Slideshow : MonoBehaviour {

	[SerializeField]
	Sprite[] images;
	SpriteRenderer rend;
	int which;
	[SerializeField]
	GameObject[] text;

	private void Start()
	{
		rend = GetComponent<SpriteRenderer>();
		rend.sprite = images[0];
		which = 0;
		text[which].SetActive(true);
	}

	void Update () {
		if (Input.anyKeyDown)
		{
			which++;
			if (which == images.Length)
			{
				SceneManager.LoadScene(1);
			}
			else
			{
				text[which - 1].SetActive(false);
				text[which].SetActive(true);
				rend.sprite = images[which];

			}
		}
	}
}
