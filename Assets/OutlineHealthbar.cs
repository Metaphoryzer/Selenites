﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineHealthbar : MonoBehaviour {
    [SerializeField]
    Animator myAnimator, parentAnimator;
    SpriteRenderer myRend, parentRend;

    [SerializeField]
    Color lowHpColor, fullHpColor;
    Color currentHpColor;

    [SerializeField]
    Health health;
    float healthPercentage;
    // Use this for initialization
    void Awake () {
        myAnimator = GetComponent<Animator>();
        parentAnimator = transform.parent.GetComponent<Animator>();
        myRend = GetComponent<SpriteRenderer>();
        parentRend = transform.parent.GetComponent<SpriteRenderer>();
        health = transform.parent.GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
        healthPercentage = health.currentHealth / 100;
        myAnimator.SetBool("OnGround", parentAnimator.GetBool("OnGround"));
        myAnimator.SetBool("Running", parentAnimator.GetBool("Running"));
        myAnimator.SetBool("Falling", parentAnimator.GetBool("Falling"));
        myAnimator.SetBool("Jump", parentAnimator.GetBool("Jump"));
        //if (parentAnimator.GetBool("Throw")) myAnimator.SetTrigger("Throw");
        myAnimator.SetBool("Die", parentAnimator.GetBool("Die"));
        myRend.flipX = parentRend.flipX;
        currentHpColor = Color.Lerp(lowHpColor, fullHpColor, healthPercentage);
        myRend.color = currentHpColor;
        myRend.enabled = parentRend.enabled;
    }
}
