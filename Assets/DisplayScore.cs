﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayScore : MonoBehaviour {

    bool score;

    int value;
    TMPro.TextMeshProUGUI textmesh;
	// Use this for initialization
	void Awake () {
        textmesh = GetComponent<TMPro.TextMeshProUGUI>();
        textmesh.isOverlay = true;
        if (gameObject.CompareTag("Score"))
        {
            score = true;
        }
        else if (gameObject.CompareTag("Combo")) {
            score = false;
        }
        //UpdateScore();
	}
    private void Start()
    {
        if (score)
        {
            UpdateScore();
        }
        else {
            UpdateCombo();
        }
    }

    // Update is called once per frame
    void Update () {
        if (score && value != Gamemanager.Score)
        {
            UpdateScore();
        }
        else if (!score && value != Gamemanager.ShieldBounces) {
            UpdateCombo();
        }
	}

    void UpdateScore() {
        value = Gamemanager.Score;
        textmesh.text = "Score: " + value;
    }

    void UpdateCombo() {
        value = Gamemanager.ShieldBounces;
        textmesh.text = "Dmg X " + value;
    }
}
