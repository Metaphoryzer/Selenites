﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TEST : MonoBehaviour {

    public Transform dotTransform;

    [Range(-1, 1)]
    public float horizontal, vertical;

    float xEdge, xcenter, yEdge, ycenter;

    Vector3 toColEdge, colCenter;

    BoxCollider2D _col;
	
	void Update ()
    {
        if(_col == null)
        {
            _col = GetComponent<BoxCollider2D>();
        }

        // x axis
        xcenter = _col.offset.x * transform.localScale.x;
        xEdge = transform.localScale.x * _col.size.x * horizontal * 0.5f;

        // y axis
        ycenter = _col.offset.y * transform.localScale.y;
        yEdge = transform.localScale.y * _col.size.y * vertical * 0.5f;

        // from global to local transform
        colCenter = transform.up * ycenter + transform.right * xcenter;
        toColEdge = transform.up * yEdge + transform.right * xEdge;
        colCenter.z = -1;

        // set pos
        dotTransform.position = transform.position + colCenter + toColEdge;



        //Physics2D.CircleCast()
	}
}
