﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnlockLvl2 : MonoBehaviour {
    Button btn;
    Image img;
    TextMeshProUGUI text;
    Color original;

    private void Awake()
    {
        btn = GetComponent<Button>();
        img = GetComponent<Image>();
        text = GetComponentInChildren<TextMeshProUGUI>();
        btn.enabled = false;
        img.enabled = false;
        original = text.color;
        text.color = Color.grey;
    }


    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetInt("stageUnlocked") == 1) {
            btn.enabled = true;
            img.enabled = true;
            text.color = original;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
