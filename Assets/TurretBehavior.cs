﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehavior : MonoBehaviour {
    [SerializeField]
    GameObject rootObject;
    bool rootExists = false;
    [SerializeField]
    LayerMask ground;

    float timer = 0, hostilityResetTimer = 0;
    [SerializeField]
    float shockwaveFrequency;
    bool cooldown = false;
    Vector3 playerPos;

    ObjectPool objPool;
    [SerializeField]
    List<GameObject> shockwaves;
    List<Shockwave> shockwaveFunctions;
    int poolIndex = 0;
    bool readyToShoot;
    bool Hostile { get; set; }
    private AudioSource sound;

    Animator anim;

    void Awake () {
        objPool = GetComponent<ObjectPool>();
        shockwaves = new List<GameObject>(objPool.PoolSize);
        shockwaveFunctions = new List<Shockwave>(objPool.PoolSize);
        if (rootObject != null) rootExists = true;
        Hostile = true;
        //ground = 8;
        anim = GetComponent<Animator>();
        anim.SetBool("Hostile", true);
        anim.SetBool("Fire", false);
        sound = GetComponent<AudioSource>();

    }

    private void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            GameObject curChild = gameObject.transform.GetChild(i).gameObject;
            curChild.SetActive(false);
            shockwaveFunctions.Add(curChild.GetComponent<Shockwave>());
            shockwaves.Add(curChild);
        }
        
    }


    void Update ()
    {
        if (rootExists) {

            if (transform.position != rootObject.transform.position && transform.rotation != rootObject.transform.rotation) {
                transform.position = rootObject.transform.position;
                transform.rotation = rootObject.transform.rotation;
            }
           
        }
        DistanceCheck();
        hostilityResetTimer = (Hostile ? 0 : hostilityResetTimer + Time.deltaTime);

        if (hostilityResetTimer > 5) {
            Hostile = true;
            anim.SetBool("Hostile", true);

        }
        
    }

    private void DistanceCheck()
    {
        playerPos = Gamemanager.PlayerTransform.position;

        if (Mathf.Abs(playerPos.x - transform.position.x) < 5 && Mathf.Abs(playerPos.y - transform.position.y) < 5 && !cooldown)
        {
            if (Hostile)
            {
                anim.Play("HostileFire");
            }
            else
            {
                anim.Play("FriendlyFire");
            }

            ShootShockwaves();   
        }

        if (cooldown) timer += Time.deltaTime;

        if (timer >= shockwaveFrequency)
        {
            timer = 0;
            cooldown = false;
            
        }

    }

    void ShootShockwaves() {

        sound.PlayOneShot(sound.clip);

        if (poolIndex + 1 >= shockwaves.Count) poolIndex = 0;

        RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, -transform.right, 1f, ground);
        RaycastHit2D groundCheckLeft = Physics2D.Raycast(transform.position - transform.right, -transform.up, .5f, ground);
        
        RaycastHit2D hitRight = Physics2D.Raycast(transform.position, transform.right, 1f, ground);
        RaycastHit2D groundCheckRight = Physics2D.Raycast(transform.position + transform.right, -transform.up, .5f, ground);
        if (hitLeft.collider == null && groundCheckLeft.collider != null) {
            
            shockwaveFunctions[poolIndex].Activate(Hostile);
            shockwaveFunctions[poolIndex].SetDirection(-1);
        }


        if (hitRight.collider == null && groundCheckRight.collider != null) {
     
            shockwaveFunctions[poolIndex + 1].Activate(Hostile);
            shockwaveFunctions[poolIndex + 1].SetDirection(1);
        }
  
        cooldown = true;
        poolIndex += 2;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ShieldProjectile")) {
            Hostile = false;
            anim.SetBool("Hostile", false);
            hostilityResetTimer = 0;
        }
    }
}
