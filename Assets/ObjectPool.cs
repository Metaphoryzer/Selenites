﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {
    [SerializeField]
    int poolSize;
    public int PoolSize { get { return poolSize; } }

    [SerializeField]
    GameObject poolObject;

    public List<GameObject> pool;
    


    private void Awake()
    {
        pool = new List<GameObject>(poolSize);
        if (poolObject != null)
        {
            for (int i = 0; i < poolSize; i++)
            {
                pool.Add(poolObject);
            }
        }

        for (int i = 0; i < poolSize; i++) {
            GameObject obj = Instantiate(pool[i], transform.position, Quaternion.identity);
            obj.transform.SetParent(gameObject.transform);
            //obj.transform.rotation = transform.parent.rotation;
        }
    }
}
