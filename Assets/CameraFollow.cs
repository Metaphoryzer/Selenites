﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    Camera cam;

    [SerializeField, Tooltip("The object to follow, ie. the player")]
    GameObject target;
    [SerializeField, Tooltip("The projectile the player shoots")]
    GameObject projectile;
    [SerializeField]
    LayerMask ground;
    Vector3 targetPos;
    Vector3 positionDifference;

    [SerializeField]
    float zoomThreshold;

    float curFrameSize, prevFrameSize;
    [SerializeField, Tooltip("Height offset is used to bring the camera upwards so the player character is slightly off center for better visibility.")]
    float cameraHeightOffset;
    //Camera speed multiplier is used to keep the character in the frame properly at all times
    float cameraSpeedMultiplier;
    float defaultCSM = 4.4f;

    float zoomSpeedMultiplier;
    [SerializeField, Tooltip("Multiplier extremes for the zoom speed. Minimum used when zooming in, maximum when zooming out.")]
    float minSpeed = 1, maxSpeed = 3;


    [SerializeField, Tooltip("Minimum and maximum zoom levels of the camera (modifies the orthographic size of the camera)")]
    float minZoom = 5, maxZoom = 15;

    Vector3 velocity;
    float zoomVelocity, zoomStartTimer;
    Vector3 prevFramePos;

    Movement playerMovement;

	// Use this for initialization
	void Awake () {
        cam = GetComponent<Camera>();
        Gamemanager.Camera = cam;
        if (target != null) {
            playerMovement = target.GetComponent<Movement>();
            transform.position = target.transform.position + Vector3.back * 100;
        }
        cameraSpeedMultiplier = defaultCSM;
        zoomStartTimer = 0;
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(new Vector3(Gamemanager.CamBoundsX.x, Gamemanager.CamBoundsY.x), 1f);
        Gizmos.DrawWireSphere(new Vector3(Gamemanager.CamBoundsX.y, Gamemanager.CamBoundsY.y), 1f);
        //Gizmos.DrawLine(target.transform.position, );
    }

    // Update is called once per frame
    void LateUpdate () {

        if (target != null) {

            curFrameSize = cam.orthographicSize;
            //if ((projectile.activeSelf && (!Gamemanager.ShieldVisible) || !Gamemanager.PlayerVisible))
            if (projectile.activeSelf)
            {
                
                //if the camera was larger during the previous frame than during the current frame, we are zooming in so zoom speed is set to the minimum value...
                if (prevFrameSize > curFrameSize)
                {
                    zoomSpeedMultiplier = minSpeed;
                }
                //.. else we are zooming out. We want to zoom out at a faster rate to keep track of the projectile
                else
                {
                    zoomSpeedMultiplier = maxSpeed;
                }

                //zoom in or out depending on the distance and if there are walls between the player and the projectile

                
                RaycastHit2D hit = Physics2D.Linecast(target.transform.position + Vector3.up * .7f, projectile.transform.position, ground);

               
                if (hit.collider != null)
                {
                    cam.orthographicSize = Mathf.SmoothDamp(curFrameSize, maxZoom, ref zoomVelocity, Time.deltaTime * cameraSpeedMultiplier * 5);
                    
                }
                else
                {
                    float curZoom = (Mathf.Abs(positionDifference.x) > Mathf.Abs(positionDifference.y) ? Mathf.Abs(positionDifference.x) : Mathf.Abs(positionDifference.y));

                    curZoom = (Mathf.Abs(Gamemanager.CamBoundsY.x) + Mathf.Abs(Gamemanager.CamBoundsY.y)) / maxZoom * curZoom;

                    cam.orthographicSize = Mathf.SmoothDamp(curFrameSize, Mathf.Clamp(curZoom, minZoom, maxZoom), ref zoomVelocity, Time.deltaTime * cameraSpeedMultiplier * 5);

                }

                //if the projectile has been shot, the target position of the camera is set to be between the projectile and the player character, otherwise it's set to follow the player
                
                positionDifference = new Vector3(projectile.transform.position.x - target.transform.position.x, projectile.transform.position.y - target.transform.position.y, -100);
                if (positionDifference.magnitude > curFrameSize * cam.aspect || positionDifference.magnitude > curFrameSize - cameraHeightOffset)
                {
                    targetPos = target.transform.position + positionDifference / 2;
                }
                else
                {
                    targetPos = new Vector3(target.transform.position.x + playerMovement.getCurrentVelocity().x , target.transform.position.y + playerMovement.getCurrentVelocity().y / cameraSpeedMultiplier + cameraHeightOffset, -100);
                }
                
            }
  
            else
            {
                targetPos = new Vector3(target.transform.position.x + playerMovement.getCurrentVelocity().x / cameraSpeedMultiplier, target.transform.position.y + playerMovement.getCurrentVelocity().y / cameraSpeedMultiplier + cameraHeightOffset, -100);

                // cam.orthographicSize = Mathf.SmoothDamp(cam.orthographicSize, minZoom, ref zoomVelocity, Time.deltaTime * Vector2.Distance(transform.position, target.transform.position));
                cam.orthographicSize = Mathf.SmoothDamp(curFrameSize, minZoom, ref zoomVelocity, Time.deltaTime * cameraSpeedMultiplier * 2);

                //transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * cameraSpeedMultiplier * 2);
            }

 
            //Clamp camera target position to be within the level bounds
            targetPos = new Vector3(Mathf.Clamp(targetPos.x, Gamemanager.CamBoundsX.x, Gamemanager.CamBoundsX.y), Mathf.Clamp(targetPos.y, Gamemanager.CamBoundsY.x, Gamemanager.CamBoundsY.y), targetPos.z);

            //cameraSpeedMultiplier = cameraSpeedMultiplier / Vector2.Distance(target.transform.position, projectile.transform.position);
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, cameraSpeedMultiplier * 2 * Time.deltaTime);
            
            

            if (curFrameSize < minZoom) cam.orthographicSize = minZoom;

        }

    }
	
}
