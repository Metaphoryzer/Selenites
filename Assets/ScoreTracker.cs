﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTracker: MonoBehaviour {

    private static ScoreTracker instance = null;

    //Score joka pitää ylittää että toinen pelattava taso aukeaa
    [SerializeField]
    int unlockThreshold = 500;

    public List<int> scores;
    bool playerDead;
    public static bool scoreSet;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this) {
            Destroy(gameObject);
            return;
        }


        scores = new List<int>();
        //PlayerPrefs.DeleteAll();
        
        GetExistingScores();
       
    }

    private void Start()
    {

        scoreSet = false;
        playerDead = Gamemanager.PlayerDead;
    }

    private void Update()
    {
        if (playerDead != Gamemanager.PlayerDead) playerDead = Gamemanager.PlayerDead;

        if (playerDead && !scoreSet) {
            SetNewHighScores();
        }
    }

    void GetExistingScores() {

        if (PlayerPrefs.HasKey("score1"))
        {
            scores.Add(PlayerPrefs.GetInt("score1"));
        }
        else {
            scores.Add(0);
            PlayerPrefs.SetInt("score1", 0);
        }


        if (PlayerPrefs.HasKey("score2"))
        {
            scores.Add(PlayerPrefs.GetInt("score2"));
        }
        else
        {
            scores.Add(0);
            PlayerPrefs.SetInt("score2", 0);
        }


        if (PlayerPrefs.HasKey("score3"))
        {
            scores.Add(PlayerPrefs.GetInt("score3"));
        }
        else
        {
            scores.Add(0);
            PlayerPrefs.SetInt("score3", 0);
        }


        if (!PlayerPrefs.HasKey("stageUnlocked")) PlayerPrefs.SetInt("stageUnlocked", 0);
    }

    void SetNewHighScores()
    {
        scores.Sort();
        scores.Reverse();

        for (int i = 0; i < scores.Count; i++) {
            if (Gamemanager.Score > scores[i]) {
                for (int j = scores.Count - 1; j > i; j--) {
                    scores[j] = scores[j - 1];
                }
                scores[i] = Gamemanager.Score;
                break;
            }
            
        }

        scores.Sort();
        scores.Reverse();

        for (int j = 0; j < scores.Count; j++) {
            Debug.Log("scores[" + j + "]: " + scores[j]);
        }

        if (scores[0] > PlayerPrefs.GetInt("score1")) PlayerPrefs.SetInt("score1", scores[0]);
        if (scores[1] > PlayerPrefs.GetInt("score2")) PlayerPrefs.SetInt("score2", scores[1]);
        if (scores[2] > PlayerPrefs.GetInt("score3")) PlayerPrefs.SetInt("score3", scores[2]);
        scoreSet = true;


        if (PlayerPrefs.GetInt("score1") > unlockThreshold && PlayerPrefs.GetInt("stageUnlocked") == 0) {
            Gamemanager.ShowUnlockBanner = true;
            PlayerPrefs.SetInt("stageUnlocked", 1);
        }

        PlayerPrefs.Save();
    }
}
